<?php namespace App\Model;


use Faker\Factory;
use Nette\Database\Context;
use Nette\Database\Table\Selection;
use Tracy\Debugger;

require_once(__DIR__ . '/../../vendor/autoload.php');

class UserRepository
{
    private $database;
    private $table;

    public function __construct(Context $database)
    {
        $this->database = $database;
        $this->table = 'user';
    }

    private function generateFakeUser()
    {
        $faker = Factory::create();
        $out = array();

        $out['first_name'] = $faker->firstName;
        $out['last_name'] = $faker->lastName;
        $out['email'] = $faker->email;
        $out['role'] = $faker->randomElement(array('admin', 'developer', 'user', 'tester'));
        $out['department'] = $faker->randomElement(array('sales', 'development'));
        $out['age'] = $faker->numberBetween(18, 80);
        $out['address'] = $faker->address;

        return $out;
    }

    public function getAll(array $columns, $filter, $sorting, $page, $itemsPerPage)
    {
        $columns[] = 'id';

        $users = $this->database->table($this->table)
            ->select(implode(',', $columns))
            ->where('active', 1);

        $this->filterUsers($users, $filter);
        $this->sortUsers($users, $sorting);

        $users->page($page, $itemsPerPage);

//        $sql = $users->getSql();
//        var_dump($sql);
//        exit;

        return $users;
    }

    public function generateDatabaseUsers($count)
    {
        Debugger::timer();
        echo '* User generation Start * <br />';
        echo 'Generating ...<br />';

        while ($count >= 1) {
            $user = $this->generateFakeUser();
            $this->database->table($this->table)->insert($user);
            $count--;
        }

        $time = Debugger::timer();
        echo '* User generation Stop. Duration: ' . $time . 's. *';
        exit;
    }

    private function filterUsers($users, $filter)
    {
        if(!empty($filter)) {
            foreach($filter as $column => $content) {
                $users->where("$column LIKE ?", '%' . $content . '%');
            }
        }
    }

    private function sortUsers(Selection $users, $sorting)
    {
        if(isset($sorting['sort_by']) && !empty($sorting['sort_by']) && isset($sorting['sort_direction']) && !empty($sorting['sort_direction'])) {
            $users->order($sorting['sort_by'] . ' ' . $sorting['sort_direction']);
        }
    }

    public function getItemsCount($filter)
    {
        $users = $this->database->table($this->table)
            ->where('active', 1);

        $this->filterUsers($users, $filter);

        return $users->count();
    }

    public function fetchById($userId)
    {
        return $this->database->table($this->table)
            ->where('active', 1)
            ->get($userId);
    }

}