<?php namespace App\Controller;


use App\Config;
use App\Helpers\Filter;
use App\Helpers\Paginator;
use App\Helpers\Sorting;
use App\Helpers\Utils;
use App\Model\UserRepository;
use Latte\Engine;

require_once(__DIR__ . '/../../vendor/autoload.php');

class HomeController
{
    public function start(Config $config, UserRepository $userRepository)
    {
        // router substitution :)
        if(isset($_GET['action']) && $_GET['action'] === 'getUserInfo') {
            $this->getUserInfo($userRepository, $_GET);
            exit;
        }

        $latte = new Engine();
        $sortingHelper = new Sorting();
        $filterHelper = new Filter();
        $columnsToShow = array('first_name', 'last_name', 'email', 'role', 'department');

        $latte->setTempDirectory($config->getCacheDirectory());

        $parameters = array();
        $parameters['baseUrl'] = $config->getBaseUrl();
        $parameters['columnsToShow'] = $columnsToShow;
        $parameters['filter'] = $filterHelper->getFilter($_GET, $columnsToShow);
        $parameters['sorting'] = $sortingHelper->getSorting($_GET);
        $parameters['actualUrl'] = Utils::getActualUrl($config->getBaseUrl(), $parameters['filter'], $parameters['sorting']);
        $parameters['sortingHelper'] = $sortingHelper;

        $paginator = new Paginator($itemsPerPage = 10, $_GET);
        $paginator->setItemsCount($userRepository->getItemsCount($parameters['filter']));
        $parameters['pagesCount'] = $paginator->getPagesCount();
        $parameters['paginator'] = $paginator;

        $parameters['users'] = $userRepository->getAll($columnsToShow, $parameters['filter'], $parameters['sorting'], $paginator->getActualPage(), $paginator->getItemsPerPage());

        $latte->render($config->getTemplateDirectory() . '/home.latte', $parameters);
    }

    private function getUserInfo(UserRepository $userRepository, $get)
    {
        $userId = intval($get['userid']);
        if(isset($get['userid']) && !empty($userId)) {
            $userId = (int)$get['userid'];
            $userData = $userRepository->fetchById($userId);

            if($userData) {
                echo json_encode($userData->toArray());
            } else {
                echo json_encode(array('status' => 'fail', 'msg' => 'User Not Found.'));
            }
        }
    }
}