<?php


namespace App;

require_once(__DIR__ . '/../vendor/autoload.php');


class Config
{
    private $templateDirectory;
    private $cacheDirectory;
    private $dsn;
    private $dbname;
    private $host;
    private $dbpassword;
    private $dbuser;
    private $baseUrl;

    public function __construct()
    {
        $this->setTemplateDirectory(__DIR__ . '/View');
        $this->setCacheDirectory(__DIR__ . '/../Temp/cache');
        $this->setDbname('zadmin_switch');
        $this->setDbpassword('evarynyhu');
        $this->setHost('localhost');
        $this->setDbuser('switch');
        $this->setBaseUrl('http://switch.sajgal.com/');

        $this->setDsn($this->getDbname(), $this->getHost());
    }

    /**
     * @return mixed
     */
    public function getTemplateDirectory()
    {
        return $this->templateDirectory;
    }

    /**
     * @param mixed $templateFolder
     */
    public function setTemplateDirectory($templateFolder)
    {
        $this->templateDirectory = $templateFolder;
    }

    /**
     * @return mixed
     */
    public function getCacheDirectory()
    {
        return $this->cacheDirectory;
    }

    /**
     * @param mixed $cacheDirectory
     */
    public function setCacheDirectory($cacheDirectory)
    {
        $this->cacheDirectory = $cacheDirectory;
    }

    /**
     * @return mixed
     */
    public function getDsn()
    {
        return $this->dsn;
    }

    /**
     * @param string $host
     * @param $dbname
     * @internal param mixed $dsn
     */
    public function setDsn($dbname, $host = "localhost")
    {
        $this->dsn = "mysql:host=$host;dbname=$dbname";
    }

    /**
     * @return mixed
     */
    public function getDbname()
    {
        return $this->dbname;
    }

    /**
     * @param mixed $dbname
     */
    public function setDbname($dbname)
    {
        $this->dbname = $dbname;
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return mixed
     */
    public function getDbpassword()
    {
        return $this->dbpassword;
    }

    /**
     * @param mixed $dbpassword
     */
    public function setDbpassword($dbpassword)
    {
        $this->dbpassword = $dbpassword;
    }

    /**
     * @return mixed
     */
    public function getDbuser()
    {
        return $this->dbuser;
    }

    /**
     * @param mixed $dbuser
     */
    public function setDbuser($dbuser)
    {
        $this->dbuser = $dbuser;
    }

    /**
     * @return mixed
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @param mixed $baseUrl
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }
}