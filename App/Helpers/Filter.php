<?php namespace App\Helpers;


class Filter
{
    public function getFilter($get, $columns)
    {
        $out = array();

        foreach ($columns as $column) {
            if(isset($get[$column]) && !empty($get[$column])) {
                $out[$column] = $get[$column];
            }
        }

        return $out;
    }
}