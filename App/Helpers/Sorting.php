<?php namespace App\Helpers;


class Sorting
{
    public function getSortingUrl($baseUrl, $filters, $page, $sortColumn, $sorting)
    {
        $out = Utils::getActualUrl($baseUrl, $filters, array(), $page);

        $out .= '&sort_by=' . $sortColumn . '&sort_direction=';

        if (isset($sorting['sort_by']) && $sorting['sort_by'] == $sortColumn) {
            if ($sorting['sort_direction'] == 'ASC') {
                $out .= 'DESC';
            } else {
                $out .= 'ASC';
            }
        } else {
            $out .= 'ASC';
        }

        return $out;
    }

    public function getSorting($get)
    {
        $out = array();

        if(isset($get['sort_by']) && !empty($get['sort_by']) && isset($get['sort_direction']) && !empty($get['sort_direction'])) {
            $out['sort_by'] = $get['sort_by'];
            $out['sort_direction'] = $get['sort_direction'];
        }

        return $out;
    }
}