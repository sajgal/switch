<?php namespace App\Helpers;


class Utils
{
    public static function getActualUrl($url, $filter, $sorting = array(), $page = NULL)
    {
        $get = array_merge($filter, $sorting);

        if($page) {
            $get = array_merge($get, array('page' => $page));
        }

        return $url . '?' . http_build_query($get);
    }
}