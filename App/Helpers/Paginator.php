<?php


namespace App\Helpers;


class Paginator
{
    private $itemsPerPage;
    private $itemsCount;
    private $pagesCount;
    private $get;

    public function __construct($itemsPerPage = 10, $get)
    {
        $this->setItemsPerPage($itemsPerPage);
        $this->get = $get;
    }

    /**
     * @return int
     */
    public function getItemsPerPage()
    {
        return $this->itemsPerPage;
    }

    /**
     * @param int $itemsPerPage
     */
    public function setItemsPerPage($itemsPerPage)
    {
        $this->itemsPerPage = $itemsPerPage;
    }

    public function getOffset()
    {
        return 0;
    }

    public function getLimit()
    {
        return $this->getItemsPerPage();
    }

    /**
     * @return mixed
     */
    public function getItemsCount()
    {
        return $this->itemsCount;
    }

    /**
     * @param mixed $itemsCount
     */
    public function setItemsCount($itemsCount)
    {
        $this->itemsCount = $itemsCount;
        $this->setPagesCount($itemsCount/$this->getItemsPerPage());
    }

    /**
     * @return mixed
     */
    public function getPagesCount()
    {
        return (int)$this->pagesCount;
    }

    /**
     * @param mixed $pagesCount
     */
    public function setPagesCount($pagesCount)
    {
        $this->pagesCount = $pagesCount;
    }

    public function getActualPage()
    {
        if(isset($this->get['page']) && !empty($this->get['page'])) {
            return (int)$this->get['page'];
        }

        return 1;
    }

    public function isActualPage($pageNumber)
    {
        return $pageNumber == $this->getActualPage()? true : false;
    }

    public function isLast()
    {
        return $this->getActualPage() == $this->getPagesCount() ? true : false;
    }

    public function isFirst()
    {
        return $this->getActualPage() == 1 ? true : false;
    }

    public function getNextPageNumber()
    {
        if($this->isLast()) { return false; }
        return $this->getActualPage()+1;
    }

    public function getPrevPageNumber()
    {
        if($this->isFirst()) { return false; }
        return $this->getActualPage()-1;
    }

    public function getPaginatorUrl($baseUrl, $actualUrl, $direction, $forcePageNumber = NULL)
    {
        $urlPart = $baseUrl . '?';
        $pageNumber = $this->getNextPageNumber();

        if ($baseUrl !== $actualUrl) {
            $urlPart = $actualUrl . '&';
        }

        if($direction == 'prev') {
            $pageNumber = $this->getPrevPageNumber();
        }

        if(isset($forcePageNumber)) {
            $pageNumber = $forcePageNumber;
        }

        return $urlPart . 'page=' . $pageNumber;
    }
}