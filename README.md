# Switch.tv Users #

To demonstrate my skills in AJAX, MySQL, PHP and JS/jQuery I'am not using any PHP framework. But for confident development, I'am using few tools, witch helped me write better & cleaner code.

### Tools I'am using ###

* [Composer](https://getcomposer.org/)(Dependencies management, Autoloader)
* [Latte](http://latte.nette.org/) (template engine like Twig or Blade)
* [Nette Database](https://github.com/nette/database) (database layer - PDO)
* [Git](bitbucket.org)
* [Faker](https://github.com/fzaninotto/Faker)(I write simple script that helps me to fill database with fake data: [generateDatabaseUsers()](https://bitbucket.org/sajgal/switch/src/a7a34adca255c2b0958e4d3ace4b521ca6b30fa6/App/Model/UserRepository.php?at=master#cl-58) )


### Security ###
Yes, I'am using unsanitize global variable $_GET in HomeController.php, but it's secure. Latte provides escaping to template output and Nette Database provides security on SQL requests.

Yes, I'am commiting config file to git. It's not right but it's only for this time. I swear.


### What's not perfect ###
Because of lack of time, this solution is not universal. There's some sections, that are not reusable, but I tried to separate database request, business logic and output generation.

### Usability ###
All (sorting, filtering & pagination) is presented in URL get parameters. It's convenient, when I can send URL to other person. Sorting is persistent. It's only repealed, when the table is filtered.