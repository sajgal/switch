<?php

use App\Config;
use App\Model\UserRepository;
use Nette\Database\Connection;
use Nette\Database\Context;
use Tracy\Debugger;

include_once(__DIR__ . '/vendor/autoload.php');
Debugger::enable(Debugger::DEVELOPMENT);

//--- this part should manage some Dependency Injection Container ---
$config = new Config();
$connection = new Connection($config->getDsn(), $config->getDbuser(), $config->getDbpassword());
$database = new Context($connection);
$userRepository = new UserRepository($database);
//-------------------------------------------------------------------

//--- User Generation Helper ----------------------------------------
// $userRepository->generateDatabaseUsers(50);
//-------------------------------------------------------------------

$app = new \App\Controller\HomeController();
$app->start($config, $userRepository);